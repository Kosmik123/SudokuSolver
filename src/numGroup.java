
// reprezentuje jedn� grup�: kwadrat lub rz�d lub kolumn�
public class numGroup {
	
	String type;
	numCell[] members;

	numGroup(String newType,numCell[] membs) 
	{
		this.type = newType;
		this.members = new numCell[9];
		for (int i = 0; i < 9; i++) {
			this.members[i] = membs[i];
		}
	}

	numGroup(String newType, numCell memb1, numCell memb2, numCell memb3, numCell memb4, numCell memb5, 
			numCell memb6, numCell memb7, numCell memb8, numCell memb9) 
	{
		this.members = new numCell[9];
		this.members[0] = memb1;
		this.members[1] = memb2;
		this.members[2] = memb3;
		this.members[3] = memb4;
		this.members[4] = memb5;
		this.members[5] = memb6;
		this.members[6] = memb7;
		this.members[7] = memb8;
		this.members[8] = memb9;
		this.type = newType;
	}
	
	// metoda sprawdza jakie liczby s� w danej grupie i te kt�re
	// wyst�puj� usuwa z listy mo�liwych liczb swoich cz�onk�w numCell
	boolean checkRepeatedNumbers() {
		boolean fDone = false;
		for (int k = 0; k < 9; k++) {
			int tempVal = this.members[k].value;
			// System.out.println(" Czy moja "+k+"-a kom�rka ma warto��?");
			if (tempVal != 0) {
				// System.out.println(" Tak, ma "+tempVal);
				// System.out.println(" Usuwam zatem.");
				for (int i = 0; i < 9; i++) {
					// System.out.print(" Kom�rka "+i+":");
					fDone = members[i].removeProbable(tempVal);
				}
			}
		}
		if(fDone) System.out.println("Wykonano chkRepNum");
		return fDone;
	}

	// metoda sprawdza czy jest liczba kt�rej nie ma w prawdopodobie�stwach
	// �adnej kom�rki opr�cz jednej, wtedy ta jedna musi mie� t� liczb�
	boolean checkEliminatedNumbers() {
		boolean fDone = false;
		for (int li = 1; li <= 9; li++) {
			int potentialCell = -1;
			for (int k = 0; k < 9; k++) {
				if (members[k].value == 0 && members[k].containProbable(li)) {
					if (potentialCell == -1) {
						potentialCell = k;
					} else {
						potentialCell = 10;
						break;
					}
				}
			}
			if (potentialCell > -1 && potentialCell < 10) {
				members[potentialCell].setValue(li);
				fDone = true;
			}
		}
		return fDone;
	}

	boolean checkEliminatedNumbers2()
	{
		boolean fDone = false;
		System.out.println("Sprawdzam "+type);
		for(int checkedNumber = 1; checkedNumber <= 9; checkedNumber++)
		{
			boolean numberFound = false;
			int cellChosen = -1;
			//System.out.print("\tZnalaz�em tak� liczb� w: ");
			for(int cellChecked = 0; cellChecked < 9; cellChecked++)
			{
				if(members[cellChecked].value == 0)
				{
					if(members[cellChecked].containProbable(checkedNumber) && !numberFound) // znaleziono liczbe					
					{
						//System.out.print("memberze "+cellChecked);
						cellChosen = cellChecked;
						numberFound = true;
					}
					else if(members[cellChecked].containProbable(checkedNumber) && numberFound) // znaleziono liczb� drugi raz, zle
					{
						//System.out.println(" ale te� w memberze "+cellChecked);
						// numberFound = false;
						cellChosen = -1;
						break;
					}
				}
			}
			//if(!numberFound) System.out.println("�adnym memberze.");
				
			if(numberFound && cellChosen > -1)
			{
				//System.out.println("\tDlatego ustawiam membera "+cellChosen+" na "+checkedNumber);
				fDone = true;
				members[cellChosen].setValue(checkedNumber);
			}	
		}
		return fDone;
	}

	String getMemberValues() {
		String s = "";
		s += this.members[0].value();
		for (int i = 1; i < 9; i++) {
			s += ", ";
			s += this.members[i].value();
		}
		return s;
	}

};
