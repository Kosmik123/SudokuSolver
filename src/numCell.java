
public class numCell 
{
	
	public int x, y;
	int value;
	int[] probableValues;
	
	numCell(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.value = 0;
		this.createProbableValues();

	}
	
	numCell(int x, int y,int val)
	{
		this.x = x;
		this.y = y;
		this.value = val;
		this.createProbableValues(val);
	}

	void createProbableValues(int val)
	{
		if(val == 0)
			createProbableValues();
		else
		{
			this.probableValues = new int[9];
			for(int i=0;i<9;i++)
			{
				if(i+1==val) 
					this.probableValues[i] = i+1;
				else
					this.probableValues[i] = 0;
			}
		}
	}
	
	void createProbableValues()
	{
		this.probableValues = new int[9];
		for(int i=0;i<9;i++)
		{
			probableValues[i] = i+1;
		}
	}

	public void setValue(int newVal)
	{
		if(containProbable(newVal))
		{
			this.value = newVal; 
			for(int i=0;i<9;i++)
				this.probableValues[i] = 0;
			this.probableValues[newVal-1] = newVal;
		}
	}
	
	public boolean removeProbable(int val) 
	{
		boolean rDone = false;
		//System.out.println("    Przyst�puj� do czyszczenia swojej listy");
		if (this.value == 0) 
		{
			//System.out.println(" Nie mam warto�ci, wi�c mam list�");
			for (int i = 0; i < 9; i++) 
			{
				if (this.probableValues[i] == val)
				{
					this.probableValues[i] = 0;
					rDone = true;
					//System.out.println("      Usuwam z mojej listy liczb� "+val);
					//System.out.println("      Lista wygl�da tak: "+ Arrays.toString(probableValues));
				}
				//else System.out.println("    Ale nie mam na li�cie "+val);	
			}
		}
		//else System.out.println(" Ja ju� mam warto�� "+this.value);
		return rDone;
	}
	
	// sprawdza czy prawdopodobne warto�ci maj� jedn� warto�� i zwraca j� oraz ustawia jako warto��
	public int chooseValue()
	{
		int tempValue = 0;
		for(int i=0;i<9;i++)
		{
			if(this.value==0 && this.probableValues[i] != 0)
			{
				if(tempValue == 0)
				{
					tempValue = this.probableValues[i];
				}
				else
				{
					tempValue = 0;
					break;
				}
			}	
		}
		if(tempValue!=0) this.value = tempValue;
		return tempValue;
	}

	public boolean containProbable(int probableNum)
	{
		boolean contain = false;
		for(int num : this.probableValues)
		{
			if(num == probableNum)
			{
				contain = true;
				break;
			}
		}
		return contain;
	}

	
	public String value()
	{
		if(value==0)
			return " ";
		else
			return Integer.toString(value);
	}
	
	
	public String showProbables()
	{
		String result = "";
		for(int i=0;i<9;i++)
		{
			int probVal = probableValues[i];
			if(probVal>=0)
			{
				result+=Integer.toString(probVal)+", "; 
			}
		}
		return result;
	}
};
