import java.util.Arrays;
import java.util.Scanner;
import java.applet.*;


public class main 
{
	final static int showSteps = 1; // co ile kroków ma pokazywać częściowo wykonaną planszę
	
	static boolean debug = false;
	
	static int iteration = 0;
	static int repetitions = 20; // ile ma wykonać powtórzeń maksymalnie (-1 to nieskończoność)
	static int showCellX = 5;
	static int showCellY = 5;
	
	static long multiValue = 0; 
	

	static numCell[][] field; 

	static numGroup[] squares = new numGroup[9];
	static numGroup[] rows = new numGroup[9];
	static numGroup[] columns = new numGroup[9];
			
	static Applet apl;
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) 
	{		
		String command = "";
		
		createField();
		
		if(args.length > 0) fillField(args);
		else fieldInput(); 
		
		createColumns();
		createRows();
		createSquares();
		
		drawField();
		
		System.out.println("1 - Rozwiąż\n2 - Wpisz nową planszę\n3 - Sprawdź komorke");
		command = sc.nextLine();
		switch(command){
		case "1":
			solve();
			break;
		case "2":
			fieldInput();
			break;
		case "3":
			checkCell();
			break;
		}		
	}	
		
	
	
	
	static void solve()
	{
		System.out.println("Podaj limit powtórzeń.");
		repetitions = Integer.parseInt(sc.next());
		while(multiValue==0)
		{
			multiValue = 1;
			for (int i = 0; i < 9; i++) 
			{
				sc.next();
				System.out.println("Ogarniam rząd "+i);
				rows[i].checkRepeatedNumbers();
				sc.next();
				System.out.println("Ogarniam kolumnę "+i);
				columns[i].checkRepeatedNumbers();
				sc.next();
				System.out.println("Ogarniam kwadrat "+i);
				squares[i].checkRepeatedNumbers();
				
				//squares[i].checkEliminatedNumbers2();
				//rows[i].checkEliminatedNumbers2();
				//columns[i].checkEliminatedNumbers2();
				
			}

			
			numCell[][] targetField = Arrays.copyOf(field, field.length);	
			for(int a=0;a<9;a++)
			{
				for(int b=0;b<9;b++)
				{
					targetField[a][b].chooseValue();
					multiValue *= field[a][b].value;
					if(debug) System.out.println("Komórka "+a+","+b+"\t mnoże przez "+field[a][b].value);
				}
			}		
			field = Arrays.copyOf(targetField, field.length);
			
			manageIterations();			
		}
		
		
		
		
		System.out.println("\n###################\n\n\n");
		drawField();
		System.out.println("Wykonano "+iteration+" kroków.");
	}
	
	static void manageIterations()
	{
		iteration += 1;		
		if(iteration%showSteps==0)
		{
			System.out.println("\n###################\n");
			drawField();
		}
		
		if(iteration == repetitions)
		{
			multiValue = 1;
		}
	}
	
	
	static void createRows()
	{
		for(int j=0;j<9;j++)
		{
			numCell[] tempGroup = new numCell[9];
			for(int i=0;i<9;i++)
			{
				tempGroup[i] = field[j][i];
			}
			rows[j] = new numGroup("rząd", tempGroup);
		}
	}
	
	static void createColumns()
	{
		for(int i=0;i<9;i++)
		{
			numCell[] tempGroup = new numCell[9];
			for(int j=0;j<9;j++)
			{
				tempGroup[j] = field[j][i];
			}
			columns[i] = new numGroup("kolumna", tempGroup);
		}
	}
	
	static void createSquares()
	{
		for(int a=0;a<3;a++)
		{
			for(int b=0;b<3;b++)
			{
				squares[a*3+b] = new numGroup("kwadrat",
				field[a*3+0][b*3+0],field[a*3+0][b*3+1],field[a*3+0][b*3+2],
				field[a*3+1][b*3+0],field[a*3+1][b*3+1],field[a*3+1][b*3+2],
				field[a*3+2][b*3+0],field[a*3+2][b*3+1],field[a*3+2][b*3+2]);
			}
		}
	}
	
	static void createField()
	{
		field = new numCell[9][9];
	}
	
	static void fieldInput()
	{
		
		for(int j=0;j<9;j++)
		{
			System.out.println("Podaj liczby w rzędzie "+Integer.toString(j));
			String inputRow = sc.nextLine();
			for(int i=0;i<9;i++)
			{
				if(inputRow.charAt(i)==' ') field[j][i] = new numCell(i,j);
				else field[j][i] = new numCell(i,j,Integer.parseInt(""+inputRow.charAt(i)));
			}
		}
	}
	
	static void fillField(String[] lines)
	{
		for(int j=0;j<lines.length;j++)
		{
			for(int i=0;i<lines[j].length();i++)
			{
				if(lines[j].charAt(i) == ' ') field[j][i] = new numCell(i,j);
				else field[j][i] = new numCell(i,j,Integer.parseInt(""+lines[j].charAt(i)));
			}
		}
	}
	
	static void checkCell()
	{
		System.out.println("Podaj współrzędne komórki do sprawdzenia (0-8)");
		showCellX = Integer.parseInt(sc.next());
		showCellY = Integer.parseInt(sc.next());
		
		System.out.println("Prawdopodobne wartości komórki ("+showCellX + ", "+showCellY + ") to " 
				+ field[showCellX][showCellY].showProbables());
	}
	
	
	static void drawField()
	{
		for(int j=0;j<field.length;j++)
		{
			if(j%3==0 && j>0)
			{
				System.out.println(" ─────┼─────┼───── ");
			}
			for(int i=0;i<field[j].length;i++)
			{
				if(i>0 && i%3==0) System.out.print("│"); else System.out.print(" ");
				System.out.print(field[j][i].value());
			}
			System.out.println(" ");

		}
	}
	
	static boolean fieldsEqual(numCell[][] field1, numCell[][] field2)
	{
		boolean result = true;
		for(int j=0;j<9;j++)
		{
			for(int i=0;i<9;i++)
			{
				if(field1[j][i].value != field2[j][i].value)
				{
					for(int p=0;p<9;p++)
					{
						if(field1[j][i].probableValues[p] != field2[j][i].probableValues[p])
						{
							result = false;
							break;
						}
					}
				}
				if(result==false) break;
			}
			if(result==false) break;
		}
		return result;
	}
}
